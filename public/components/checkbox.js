class Checkbox extends React.Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
            css: undefined
        }
    }
    componentDidMount() {
        let style = 'state '
        switch(this.props.style) {
            case 'y': style += 'p-warning'; break;
            case 'r': style += 'p-danger'; break;
            case 'b': style += 'p-primary'; break;
            default: style += 'p-success';
        }
        this.setState({css: style})
    }
    render() {
        return (
            <div className={this.props.subBox}>
                {this.props.link && <a href={this.props.link}> 🔗</a>}
                <div className="pretty p-icon p-fill p-tada">
                    <input id={this.props.id} name={this.props.name} type="checkbox" checked={this.props.checked} onChange={this.props.handleChange}/>
                    <div className={this.state.css}>
                        <label>{this.props.label}</label>
                        
                    </div>
                </div>
            </div>
        )
    }
}