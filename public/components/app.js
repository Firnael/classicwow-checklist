class App extends React.Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
            checklist: {},
            checkboxes: [],
            pyro: undefined
        }
        this.handleChange = this.handleChange.bind(this);
        this.clear = this.clear.bind(this);
    }
    componentDidMount() {
        const checklistFromStore = JSON.parse(localStorage.getItem('checklist'))
        if(checklistFromStore !== null) {
            this.setState({ checklist: checklistFromStore })
        } else {
            this.setState({ checklist: {} })
        }
        fetch('data/data.json')
                .then(response => response.json())
                .then(data => this.setState({ checkboxes: data }));
    }
    handleChange(event) {
        const c = this.state.checklist
        const input = event.target
        c[input.name] = input.checked
        this.setState({ checklist: c })

        // Ding !
        if(input.id === 'ding' && input.checked) {
            this.setState({pyro: 'pyro'})
            setTimeout(() => { this.setState({pyro: undefined}) }, 10000);
        }

        localStorage.setItem('checklist', JSON.stringify(this.state.checklist))
    }
    clear() {
        const c = this.state.checklist
        Object.keys(c).forEach(x => c[x] = false)
        this.setState({ checklist: c })
        localStorage.setItem('checklist', JSON.stringify(this.state.checklist))
    }
    render() {
        return (
            <div className="container">
                <br/>
                <h1>Checklist <small className="text-muted">Classic WoW</small></h1>
                <hr/>
                <div className="row">
                    <div className="col">
                        {
                            this.state.checkboxes.map((check, key) =>
                                <Checkbox key={key} 
                                    id={check.id}
                                    name={`checkbox${key}`}
                                    label={check.label}
                                    link={check.link}
                                    checked={this.state.checklist[`checkbox${key}`]}
                                    style={check.style}
                                    subBox={check.subBox}
                                    handleChange={this.handleChange}/>)
                        }
                    </div>
                    <div className="col">
                        <button onClick={() => this.clear()} type="button" className="btn btn-outline-danger">Clear</button>
                    </div>
                </div>
                <div className={this.state.pyro}>
                    <div className="before"></div>
                    <div className="after"></div>
                </div>
            </div>
        )
    }
}